import { defineConfig } from 'vite'
import { resolve } from 'path'
import vue from '@vitejs/plugin-vue'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'

const src = resolve(__dirname, 'src')

export default defineConfig({
  server: {
    host: '0.0.0.0',
    port: 5173,
    open: true
  },
  plugins: [
    vue(),
    AutoImport({
      imports: ['vue'],
      dts: resolve(src, 'types', 'auto-imports.d.ts')
    }),
    Components({
      resolvers: [ElementPlusResolver({ importStyle: 'sass' })],
      dts: resolve(src, 'types', 'components.d.ts')
    })
  ]
})
